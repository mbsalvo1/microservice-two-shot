from django.db import models


class BinVO(models.Model):
    import_href = models.CharField(max_length=150, null= True, blank=True)
    closet_name = models.CharField(max_length=150, null= True, blank=True)
    bin_number = models.PositiveIntegerField(null=True, blank=True)
    bin_size = models.PositiveIntegerField(null=True, blank=True)



class Shoe(models.Model):
    manufacturer = models.CharField(max_length=150, null=True, blank=True) 
    model_name = models.CharField(max_length=150, null=True, blank=True)
    color = models.CharField(max_length=150, null=True, blank=True)
    picture_url = models.URLField(null=True, blank=True)
    bin = models.ForeignKey("BinVO", related_name="shoes", on_delete=models.CASCADE, null=True, blank=True,)
