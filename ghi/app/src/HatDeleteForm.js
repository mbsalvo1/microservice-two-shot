import React from 'react';




class DeleteHatForm extends React.Component {
    constructor(props) {
        super(props)
        this.state = {
            ids: []
        };
        this.handleIdChange = this.handleIdChange.bind(this);
        this.handleSubmit = this.handleSubmit.bind(this);
    }

    handleIdChange(event) {
        const value = event.target.value;
        this.setState({id: value})
    }



    async handleSubmit(event) {
        event.preventDefault();
        const data = {...this.state};
        delete data.ids;
        console.log("submitted",data);

        const locationUrl = `http://localhost:8090/api/hats/${data.id}`;
        console.log(locationUrl)
        const fetchConfig = {
          method: "DELETE",
        };
        const response = await fetch(locationUrl, fetchConfig);
        if (response.ok) {
          const cleared = {
            id: ''
          };
          this.setState(cleared);
          window.location.reload(false);
          alert("Your hat was deleted");
        }
      }

    async componentDidMount(){
        const url = "http://localhost:8090/api/hats/";
        const response = await fetch(url);
        if (response.ok){
            const data  = await response.json();
            this.setState({ids: data.hats});
        }
    }
    render() {
      return (
        <div className="row">
          <div className="offset-3 col-6">
            <div className="shadow p-4 mt-4">
              <h1>Delete a new Hat</h1>
              <form onSubmit={this.handleSubmit} id="create-location-form">
                <div className="mb-3">
                  <select onChange= {this.handleIdChange} value={this.state.id} required name="id" id="id" className="form-select">
                    <option value="">Choose a Hat by Id</option>
                    {this.state.ids.map(hat => {
                        console.log(hat)
                        return (
                        <option key= {hat.style} value={hat.id}>
                        <img src= { hat.picture_url } height= "90px" width="90px" alt="hat"/> {hat.id}
                        </option>
                        );
                    })}
                  </select>
                </div>
                <button className="btn btn-primary">Delete</button>
              </form>
            </div>
          </div>
        </div>
      );
    }
  }

export default DeleteHatForm;
