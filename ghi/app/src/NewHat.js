import React from 'react';

class HatForm extends React.Component {
    constructor(props) {
        super(props)
        this.state = {
            fabric: '',
            style: '',
            color: '',
            picture_url: '',
            locations: []
        };
        this.handleFabricChange = this.handleFabricChange.bind(this);
        this.handleStyleChange = this.handleStyleChange.bind(this);
        this.handleColorChange = this.handleColorChange.bind(this);
        this.handlePictureChange = this.handlePictureChange.bind(this);
        this.handleLocationChange = this.handleLocationChange.bind(this);
        this.handleSubmit = this.handleSubmit.bind(this);
    }
    handleFabricChange(event) {
        const value = event.target.value;
        this.setState({fabric: value})
    }
    handleStyleChange(event) {
        const value = event.target.value;
        this.setState({style: value})
    }
    handleColorChange(event) {
        const value = event.target.value;
        this.setState({color: value})
    }
    handlePictureChange(event) {
        const value = event.target.value;
        this.setState({picture_url: value})
    }
    handleLocationChange(event) {
        const value = event.target.value;
        this.setState({location: value})
    }

    async handleSubmit(event) {
        event.preventDefault();
        const data = {...this.state};
        delete data.locations;
        console.log("submitted",data);

        const locationUrl = 'http://localhost:8090/api/hats/';
        const fetchConfig = {
          method: "post",
          body: JSON.stringify(data),
          headers: {
            'Content-Type': 'application/json',
          },
        };
        const response = await fetch(locationUrl, fetchConfig);
        if (response.ok) {
          const newLocation = await response.json();
          console.log(newLocation);
          const cleared = {
            fabric: '',
            style: '',
            color: '',
            picture_url: '',
            location: ''
          };
          this.setState(cleared);
          window.location.reload(false);
        }
      }

    async componentDidMount(){
        const url = "http://localhost:8100/api/locations/";
        const response = await fetch(url);
        if (response.ok){
            const data  = await response.json();
            // const selectTag = document.getElementById('state');
            // data.states.forEach(state => {
            // selectTag.innerHTML += `<option value= "${state.abbreviation}">${state.name}</option>`
            // });
            this.setState({locations: data.locations});
        }
    }
    render() {
      return (
        <div className="row">
          <div className="offset-3 col-6">
            <div className="shadow p-4 mt-4">
              <h1>Add a new Hat</h1>
              <form onSubmit={this.handleSubmit} id="create-location-form">
                <div className="form-floating mb-3">
                  <input onChange= {this.handleFabricChange} value={this.state.fabric} placeholder="Fabric" required type="text" name="fabric" id="fabric" className="form-control"/>
                  <label htmlFor="fabric">Fabric</label>
                </div>
                <div className="form-floating mb-3">
                  <input onChange= {this.handleStyleChange} value={this.state.style} placeholder="Style" required type="text" name="style" id="style" className="form-control"/>
                  <label htmlFor="style">Style</label>
                </div>
                <div className="form-floating mb-3">
                  <input onChange= {this.handleColorChange} value={this.state.color} placeholder="Color" required type="text" name="color" id="color" className="form-control"/>
                  <label htmlFor="color">Color</label>
                </div>
                <div className="form-floating mb-3">
                  <input onChange= {this.handlePictureChange} value={this.state.picture_url} placeholder="Picture"  type="url" name="picture_url" id="picture_url" className="form-control"/>
                  <label htmlFor="Picture">Picture</label>
                </div>
                <div className="mb-3">
                  <select onChange= {this.handleLocationChange} value={this.state.location} required name="location" id="location" className="form-select">
                    <option value="">Choose a Closet</option>
                    {this.state.locations.map(loc => {
                        return (
                        <option key= {loc.href} value={loc.href}>
                            {loc.closet_name}
                        </option>
                        );
                    })}
                  </select>
                </div>
                <button className="btn btn-primary">Create</button>
              </form>
            </div>
          </div>
        </div>
      );
    }
  }

export default HatForm;
