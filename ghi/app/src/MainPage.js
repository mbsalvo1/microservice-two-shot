function MainPage() {
  return (
    <div className="px-4 py-5 my-5 text-center">
      <h1 className="display-5 fw-bold">WARDROBIFY!</h1>
      <div className="col-lg-6 mx-auto">
        <p className="lead mb-4">
          Need to keep track of your shoes and hats? We have
          the solution for you!
        </p>
        <div><img src= {"https://i.pinimg.com/474x/33/7a/dc/337adc9e1c236aeb1ba26ae5a5ba8ba0--men-closet-shoe-closet.jpg" } height= "500px" width="400px" alt="hat"/></div>
      </div>
    </div>
  );
}

export default MainPage;
