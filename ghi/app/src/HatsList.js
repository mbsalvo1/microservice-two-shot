import React from 'react';
import {Link} from 'react-router-dom'





function HatsList(props) {
    // console.log("find id", props.hats[1].id)
    return (
      <div>
        <Link to="/hats/new">
          <button className="btn btn-outline-primary">Add a new Hat</button>
        </Link>
      <table className="table table-striped">

        <thead>
          <tr>
            <th>Fabric</th>
            <th>Style</th>
            <th>Color</th>
            <th>Picture</th>
            <th>Closet</th>
            <th>Hat Id</th>
          </tr>
        </thead>
        {console.log("props hats log", props.hats)}
        <tbody>
          {props.hats.map(hat => {
            return (
              <tr key={hat.id}>
                <td>{ hat.fabric }</td>
                <td>{ hat.style }</td>
                <td>{ hat.color }</td>
                <td><img src= { hat.picture_url } height= "90px" width="90px" alt="hat"/></td>
                <td>{ hat.location }</td>
                <td>{hat.id}</td>
                {/* trying to pass hat id through delete hat function  */}
                <td><button className="btn btn-outline-danger" onClick={deleteHat.bind(this,hat.id)}>Delete Hat</button></td>
              </tr>
            );
          })}
        </tbody>

      </table>
          {/* create link to delete form */}
        <Link to= '/hats/delete' ><button className="btn btn-outline-danger">Delete Hat Form</button></Link>
      </div>
    );
  }

  async function deleteHat(id) {
    fetch('http://localhost:8090/api/hats/' + id,  {
      method: 'DELETE'
    })
    alert("Your hat was deleted");
    window.location.reload(false);
  }







  export default HatsList;
