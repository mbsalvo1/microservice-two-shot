
import React from 'react';


class ShoeForm extends React.Component {
    constructor(props) {
        super(props)
        this.state = {
            manufacturer: '',
            model_name: '',
            color: '',
            picture_url: '',
            bins: [],
        };

        this.handleChangeManufacturer = this.handleChangeManufacturer.bind(this);
        this.handleChangeModelName = this.handleChangeModelName.bind(this);
        this.handleChangeColor = this.handleChangeColor.bind(this);
        this.handleChangePictureUrl = this.handleChangePictureUrl.bind(this);
        this.handleChangeBin = this.handleChangeBin.bind(this);
        this.handleSubmit = this.handleSubmit.bind(this);
    }


    handleChangeManufacturer(event) {
        const value = event.target.value;
        this.setState({ manufacturer: value });
    }
    
    handleChangeModelName(event) {
        const value = event.target.value;
        this.setState({ model_name: value });
    }
    
    handleChangeColor(event) {
        const value = event.target.value;
        this.setState({ color: value });
    }

    handleChangePictureUrl(event) {
        const value = event.target.value;
        this.setState({ picture_url: value });
    }
    
    handleChangeBin(event) {
        const value = event.target.value;
        this.setState({ bin: value });
    }


    async handleSubmit(event) {
        
        event.preventDefault();
        const data = {...this.state};
        delete data.bins;

        const url = 'http://localhost:8080/api/shoes/';
        const fetchOptions = {
            method: 'post',
            body: JSON.stringify(data),
            headers: {
                'Content-Type': 'application/json',
            },
        };
        console.log("test", data)
        const response = await fetch(url, fetchOptions);
        console.log("test-2", response)
        if (response.ok) {
            const newShoe = await response.json();

            const cleared = {
                manufacturer: '',
                model_name: '',
                color: '',
                picture_url: '',
            };
            this.setState(cleared);
        }
    }   


    async componentDidMount() {
        const url = "http://localhost:8100/api/bins/";

        const response = await fetch(url);
        console.log(response)

        if (response.ok) {
            const data = await response.json();
            this.setState({bins: data.bins});
        }
    }

    render() {
        return (
            <div className="row">
                <div className="offset-3 col-6">
                    <div className="shadow p-4 mt-4">
                        <h1>Create a new shoe</h1>
                        <form onSubmit={this.handleSubmit} id="create-bin-form">
                        <div className="form-floating mb-3">
                            <input onChange={this.handleChangeManufacturer} placeholder="Name" required type="text" name="name" id="name" className="form-control" />
                            <label htmlFor="manufacturer">Manufacturer</label>
                        </div>
                        <div className="form-floating mb-3">
                            <input onChange={this.handleChangeModelName} placeholder="Model Name" required type="text" name="model_name" id="model_name" className="form-control" />
                            <label htmlFor="model_name">Model Name</label>
                        </div>
                        <div className="form-floating mb-3">
                            <input onChange={this.handleChangeColor} placeholder="Color" required type="text" name="color" id="color" className="form-control" />
                            <label htmlFor="color">Color</label>
                        </div>
                        <div className="form-floating mb-3">
                            <input onChange={this.handleChangePictureUrl} placeholder="Picture url" required type="text" name="picture url" id="picture url" className="form-control" />
                            <label htmlFor="picture url">Picture</label>
                        </div>
                        <div className="mb-3">
                            <select onChange={this.handleChangeBin} required name="bin" id="bin" className="form-select">
                                <option value="">Choose a bin</option>
                                {this.state.bins.map(bin => {
                                    return (
                                        <option key={bin.href} value={bin.id}>{bin.closet_name}</option>
                                    )
                                    })}
                                </select>    
                            </div>
                            <button className="btn btn-primary">Create</button>
                        </form>
                    </div>
                </div>
            </div>
        );
    }
}


export default ShoeForm;
