import { BrowserRouter, Routes, Route } from 'react-router-dom';
import MainPage from './MainPage';
import Nav from './Nav';
import ShoeForm from './ShoeForm';
import BinForm from './BinForm';
import HatForm from './NewHat';
import HatsList from './HatsList';
import DeleteHatForm from './HatDeleteForm';




function App(props) {
    if (props.hats === undefined) {
      return null;
    }
  return (
    <BrowserRouter>
      <Nav />
      <div className="container">
        <Routes>
          <Route path="/" element={<MainPage />} />

          <Route path="/shoes" element={<ShoeForm />} />
          <Route path="/bins" element={<BinForm />} />

          <Route path="/hats" element={<HatsList hats={props.hats} />} />
          <Route path="/hats" element={<HatsList/>} />
          <Route path="/hats/delete" element={<DeleteHatForm/>} />
          <Route path="/hats/new" element={<HatForm/>} />

        </Routes>
      </div>
    </BrowserRouter>
  );
}


export default App;
