# Generated by Django 4.0.3 on 2022-09-09 01:06

from django.db import migrations, models
import django.utils.timezone


class Migration(migrations.Migration):

    dependencies = [
        ('hats_rest', '0003_rename_fabrick_hats_fabric_and_more'),
    ]

    operations = [
        migrations.AlterField(
            model_name='locationvo',
            name='import_href',
            field=models.CharField(default=django.utils.timezone.now, max_length=50, unique=True),
            preserve_default=False,
        ),
    ]
