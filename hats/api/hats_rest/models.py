from django.db import models
from django.urls import reverse


class LocationVO(models.Model):
    import_href = models.CharField(max_length=100, unique=True, null=True)
    closet_name = models.CharField(max_length=100)
    section_number = models.PositiveSmallIntegerField()
    shelf_number = models.PositiveSmallIntegerField()

    def __str__(self):
        return self.closet_name



class Hats (models.Model):
    fabric = models.CharField(max_length=50, null=True, blank=True)
    style = models.CharField(max_length=50, null=True, blank=True)
    color = models.CharField(max_length=20, null=True, blank=True)
    picture_url = models.URLField(null=True, blank=True)
    location = models.ForeignKey(LocationVO, related_name="locations", on_delete=models.CASCADE)

    def __str__(self):
        return self.style

    def get_api_url(self):
        return reverse("api_show_hat", kwargs={"pk": self.pk})
